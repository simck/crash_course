# Description

Learning Laravel 9 

## Requirements
XAMMP - use of Apache and MySQL


## Adding dummy data
Run Command: php artisan migrate:refresh --seed


## Credits
Crash Course: https://www.youtube.com/watch?v=MYyJ4PuL4pY

Vercel Deployment: https://github.com/vercel-community/php